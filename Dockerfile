FROM nginx:1.25.3-alpine3.18-slim as production-build
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*
COPY ./nginx/index.html /usr/share/nginx/html/
EXPOSE 80
EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]
